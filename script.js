arrastrar(document.getElementById("plaza1"));
arrastrar(document.getElementById("plaza2"));
arrastrar(document.getElementById("plaza3"));
arrastrar(document.getElementById("plaza4"));
arrastrar(document.getElementById("plaza5"));
arrastrar(document.getElementById("plaza6"));
arrastrar(document.getElementById("plaza7"));
arrastrar(document.getElementById("plaza8"));
arrastrar(document.getElementById("plaza9"));
arrastrar(document.getElementById("plaza10"));
arrastrar(document.getElementById("plaza11"));
arrastrar(document.getElementById("plaza12"));
arrastrar(document.getElementById("plaza13"));
arrastrar(document.getElementById("plaza14"));
arrastrar(document.getElementById("plaza15"));
arrastrar(document.getElementById("plaza16"));
arrastrar(document.getElementById("plaza17"));
arrastrar(document.getElementById("plaza18"));

function arrastrar(element){
    element.onpointerdown = initDrag;
    let Dx = 0;
    let Dy = 0;
    
    function initDrag(e){
        Dx= e.clientX - element.offsetLeft;
        Dy= e.clientY - element.offsetTop;
        document.onpointermove = arrastrarElement;
        document.onpointerup = stopArrastre;
    }

    function arrastrarElement (e){
        element.style.left = e.clientX - Dx +  'px';
        element.style.top = e.clientY - Dy + 'px';
    }

    function stopArrastre (e){
        document.onpointermove = null;
        document.onpointerup = null;
    }
}